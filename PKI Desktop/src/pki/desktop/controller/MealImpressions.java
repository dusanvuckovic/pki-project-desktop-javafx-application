package pki.desktop.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import org.controlsfx.control.Rating;
import pki.desktop.controller.components.CommentVisual;
import pki.desktop.controller.components.PopupImage;
import pki.desktop.model.Comment;
import pki.desktop.model.DB;
import pki.desktop.model.Restaurant;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class MealImpressions extends MealBase implements Initializable {

    @FXML
    BorderPane rootPane;

    @FXML
    private FlowPane imagePane;
    @FXML
    private FlowPane commentPane;

    @FXML
    private TextArea comment;
    @FXML
    private Rating rating;
    @FXML
    private Button commentButton;

    private void openPopup(ImageView image) {
        PopupImage popupImage = new PopupImage(rootPane.getScene().getWindow(), image);
        popupImage.togglePopup();
    }

    @FXML
    public void buttonClicked() {
        Restaurant r = DB.selectedRestaurant;
        String commentT = comment.getText().trim();
        if (!commentT.isEmpty()) {
            Comment c = new Comment(commentT, DB.loggedOn, LocalDateTime.now());
            if (!r.getComments().contains(c)) {
                r.getComments().add(c);
                commentPane.getChildren().add(new CommentVisual(c));
                comment.setDisable(true);
                commentButton.setDisable(true);
            }
        }
        if (rating.getRating() != 0) {
            r.getGrades().add((int) rating.getRating());
            restaurantTitle.setText(r.getTitle() + (!(r.getGrades().isEmpty()) ? " [" + r.getAverageGrade() + "]" : ""));
            rating.setDisable(true);
            commentButton.setDisable(true);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeStatus();
        initializePanes();
        if (!DB.isLoggedIn()) {
            comment.setDisable(true);
            commentPane.setDisable(true);
            rating.setDisable(true);
            commentButton.setDisable(true);
        }
    }

    private void initializePanes() {

        Restaurant r = DB.selectedRestaurant;
        for (ImageView img : r.getImages()) {
            ImageView iv = new ImageView(img.getImage());
            iv.setPreserveRatio(false);
            iv.setFitHeight(100);
            iv.setFitWidth(100);
            iv.setOnMouseClicked(e -> openPopup(iv));
            imagePane.getChildren().add(iv);
        }
        for (Comment c : r.getComments()) {
            CommentVisual cv = new CommentVisual(c);
            commentPane.getChildren().add(cv);
        }
    }

}
