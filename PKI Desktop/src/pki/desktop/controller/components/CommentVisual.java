package pki.desktop.controller.components;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import pki.desktop.model.Comment;

import java.time.format.DateTimeFormatter;

public class CommentVisual extends Pane {

    public CommentVisual(Comment c) {
        super();
        this.setStyle("-fx-background-color: pink;");
        this.setPrefSize(300, 100);
        this.setMaxSize(300, 100);
        this.setMinSize(300, 100);
        Label userText = new Label(c.getUsername() + " kaže:");
        Label commentText = new Label(c.getComment());
        Label dateText = new Label(c.getMadeOn().format(DateTimeFormatter.ofPattern("dd.LL.YYYY HH:mm:ss")));
        commentText.setAlignment(Pos.TOP_LEFT);
        commentText.setPrefSize(300, 80);
        commentText.setWrapText(true);
        userText.setTranslateY(0);
        commentText.setTranslateY(20);
        dateText.setTranslateY(80);
        dateText.setTranslateX(190);

        this.getChildren().addAll(userText, commentText, dateText);
    }
}
