package pki.desktop.controller.components;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import pki.desktop.model.DB;
import pki.desktop.model.Order;
import pki.desktop.model.Restaurant;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class OrderVisual extends AnchorPane {

    @FXML
    private ImageView image;
    @FXML
    private Label restaurantName;
    @FXML
    private Label status;
    @FXML
    private Label time;
    @FXML
    private Label amount;
    @FXML
    private Label address;
    @FXML
    private Label message;


    public OrderVisual(Order o) throws IOException {

        // attach FXML to this control instance
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/pki/desktop/view/components/ordervisual.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        loader.load();

        Restaurant r = o.getRestaurantDelivering();

        Image img = r.getImages().isEmpty() ? DB.NO_IMAGE.getImage() : r.getImages().get(0).getImage();
        image.setImage(img);
        image.setFitWidth(100);
        image.setFitHeight(100);
        image.setPreserveRatio(false);


        restaurantName.setText(r.getTitle());
        status.setText(o.getDescriptionFromStatus());
        status.setTextFill(getColor(o));
        time.setText(getTime(o));
        amount.setText("Račun: " + o.calculatePrice() + " RSD");
        message.setText(o.getMessageToDeliverer());
        address.setText(o.getAddress());
    }

    private String getTime(Order o) {
        switch (o.getStatus()) {
            case PENDING:
                return "Za: " + o.getRestaurantDelivering().getDeliveryTime() + " minuta";
            case FAILED:
                return "Otkazano: " + o.getDeliveredOrDeniedOn().format(DateTimeFormatter.ofPattern("HH:mm:ss dd.LL.YYYY"));
            case DELIVERED:
                return "Dostavljeno: " + o.getDeliveredOrDeniedOn().format(DateTimeFormatter.ofPattern("HH:mm:ss dd.LL.YYYY"));
        }
        return "";
    }

    private Color getColor(Order o) {
        switch (o.getStatus()) {
            case PENDING:
                return Color.BLUE;
            case FAILED:
                return Color.RED;
            case DELIVERED:
                return Color.GREEN;
        }
        return Color.DEEPPINK;
    }

}
