package pki.desktop.controller.components;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Translate;
import javafx.stage.Popup;
import javafx.stage.Window;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

public class PopupInput extends Popup {
    private final Window window;
    private boolean hidden = true;

    public PopupInput(Window window, Label label) {
        super();
        this.window = window;
        this.setHideOnEscape(true);
        this.setAutoHide(true);
        this.setHeight(100);
        this.setWidth(400);

        TextField text = new TextField("");
        text.setPromptText("Nova adresa");
        text.setTranslateX(140);
        text.setTranslateY(20);
        Glyph glyph = new Glyph("FontAwesome", FontAwesome.Glyph.CLOSE);
        glyph.setOnMouseClicked(e -> this.hidePopup());

        Rectangle background = new Rectangle(400, 80, Color.LIGHTGRAY);

        Button button = new Button("OK");
        button.getTransforms().add(Translate.translate(360, 50));
        button.setOnMouseClicked(e -> {
            if (!text.getText().trim().isEmpty())
                label.setText("Lokacija dostave: " + text.getText());
            this.hidePopup();
        });

        glyph.setFontSize(20);
        glyph.setTranslateX(370);

        this.getContent().addAll(background, glyph, text, button);
    }

    private void showPopup() {
        this.show(window);
        hidden = false;
    }

    private void hidePopup() {
        this.hide();
        hidden = true;
    }

    public void togglePopup() {
        if (hidden)
            showPopup();
        else
            hidePopup();
    }

}
