package pki.desktop.controller.components;

import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Popup;
import javafx.stage.Window;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

public class PopupImage extends Popup {

    private final ImageView image;
    private final Window window;
    private boolean hidden = true;

    public PopupImage(Window window, ImageView imageIn) {
        super();
        this.window = window;
        this.setHideOnEscape(true);
        this.setAutoHide(true);
        this.setHeight(800);
        this.setWidth(800);

        this.image = new ImageView(imageIn.getImage());
        Glyph glyph = new Glyph("FontAwesome", FontAwesome.Glyph.CLOSE);
        glyph.setOnMouseClicked(e -> this.hidePopup());

        Rectangle background = new Rectangle(800, 780, Color.LIGHTGRAY);

        glyph.setFontSize(40);
        glyph.setTranslateX(730);

        transformImage();
        this.getContent().addAll(background, glyph, this.image);
    }

    private void transformImage() {
        image.setPreserveRatio(false);
        image.setFitHeight(Math.min(730, image.getImage().getHeight()));
        image.setFitWidth(Math.min(800, image.getImage().getWidth()));

        double wf = 800, hf = 730, h = image.getFitHeight(), w = image.getFitWidth();
        image.setTranslateX((wf - w) / 2);
        image.setTranslateY(50 + hf / 2 - h / 2);
    }


    private void showPopup() {
        this.show(window);
        hidden = false;
    }

    private void hidePopup() {
        this.hide();
        hidden = true;
    }

    public void togglePopup() {
        if (hidden)
            showPopup();
        else
            hidePopup();
    }


}
