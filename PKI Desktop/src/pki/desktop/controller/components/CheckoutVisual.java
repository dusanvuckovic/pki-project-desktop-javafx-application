package pki.desktop.controller.components;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import pki.desktop.controller.Checkout;
import pki.desktop.model.DB;
import pki.desktop.model.Meal;
import pki.desktop.model.OrderedMeal;

import java.io.IOException;

public class CheckoutVisual extends AnchorPane {

    private OrderedMeal meal;
    private Checkout containingPane;


    @FXML
    private Label glyph;
    @FXML
    private ImageView image;
    @FXML
    private Label name;
    @FXML
    private Label price;
    @FXML
    private Label description;


    public CheckoutVisual(OrderedMeal meal, Checkout containingPane) throws IOException {

        // attach FXML to this control instance
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/pki/desktop/view/components/checkoutvisual.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        loader.load();

        this.meal = meal;
        this.containingPane = containingPane;
        Meal m = meal.getMeal();
        if (!m.getImages().isEmpty()) {
            image.setImage(m.getImages().get(0));
        } else
            image.setImage(DB.NO_IMAGE.getImage());
        name.setText(m.getName() + ((meal.getHowMany() != 0) ? " [" + meal.getHowMany() + "]" : ""));
        price.setText(((meal.getHowMany() != 0) ? meal.getHowMany() + " x " : "") + m.getPrice());
        description.setText(m.getCategory().getFullName());

    }

    @FXML
    private void glyphClicked() {
        containingPane.update(this);
    }

    public OrderedMeal getMeal() {
        return meal;
    }


    /*@Override
    public void initialize(URL location, ResourceBundle resources) {

    }*/
}
