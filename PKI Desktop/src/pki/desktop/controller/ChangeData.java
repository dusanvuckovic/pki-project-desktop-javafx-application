package pki.desktop.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import pki.desktop.Main;
import pki.desktop.NotificationLibrary;
import pki.desktop.model.DB;
import pki.desktop.model.User;

import java.net.URL;
import java.util.ResourceBundle;

public class ChangeData implements Initializable {

    @FXML
    TextField usernameF, firstNameF, lastNameF, phoneF, addressF;
    @FXML
    Label errorLabel;

    @FXML
    void browseClicked() {
        Main.switchScene(Main.Locations.REGISTERED_BROWSE);
    }

    @FXML
    void checkoutClicked() {
        Main.switchScene(Main.Locations.REGISTERED_ORDERS);
    }

    @FXML
    void ordersClicked() {
        Main.switchScene(Main.Locations.REGISTERED_ORDERS);
    }

    @FXML
    void changeDataClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHANGEDATA);
    }

    @FXML
    void changePassClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHANGEPASS);
    }

    @FXML
    void logoutClicked() {
        DB.logOff();
        Main.switchScene(Main.Locations.UNREGISTERED_LOGIN);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usernameF.setText(DB.loggedOn.getUsername());
        firstNameF.setText(DB.loggedOn.getFirstName());
        lastNameF.setText(DB.loggedOn.getLastName());
        phoneF.setText(DB.loggedOn.getPhone());
        addressF.setText(DB.loggedOn.getAddress());
        errorLabel.setText(" ");
    }

    @FXML
    void changeData() {
        User u = DB.loggedOn;
        String firstName = firstNameF.getText(), lastName = lastNameF.getText(),
                phone = phoneF.getText(), address = addressF.getText();

        if (firstName.trim().isEmpty() || lastName.trim().isEmpty() ||
                phone.trim().isEmpty() || address.trim().isEmpty()) {
            NotificationLibrary.notFilled().showWarning();
        } else {
            u.setFirstName(firstName);
            u.setLastName(lastName);
            u.setPhone(phone);
            u.setAddress(address);

            NotificationLibrary.changedData().showInformation();
            Main.switchScene(Main.Locations.REGISTERED_BROWSE);
        }
    }
}
