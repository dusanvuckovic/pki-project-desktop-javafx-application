package pki.desktop.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import pki.desktop.Main;
import pki.desktop.model.DB;
import pki.desktop.model.Restaurant;

import java.time.DayOfWeek;
import java.time.LocalTime;

abstract class MealBase {

    @FXML
    protected ImageView restaurantImage;
    @FXML
    protected Label restaurantTitle;
    @FXML
    protected Label restaurantDescription;
    @FXML
    protected Label restaurantStatus;
    @FXML
    protected Label restaurantWorkdayHours;
    @FXML
    protected Label restaurantSaturdayHours;
    @FXML
    protected Label restaurantSundayHours;

    private Restaurant.Hours getCurrentHours(Restaurant r) {
        DayOfWeek dow = DB.getDayOfWeek();
        int index = 0;
        if (dow == DayOfWeek.SATURDAY)
            index = 1;
        else if (dow == DayOfWeek.SUNDAY)
            index = 2;
        return r.getHours()[index];
    }

    private String getRestaurantStatus(Restaurant r) {
        LocalTime now = DB.getTime();
        Restaurant.Hours h = getCurrentHours(r);
        if ((now.isBefore(h.start)) || (now.isAfter(h.end))) {
            restaurantStatus.setTextFill(Color.RED);
            return "Zatvoreno!";
        }
        restaurantStatus.setTextFill(Color.GREEN);
        return "Otvoreno!";
    }

    void initializeStatus() {
        Restaurant r = DB.selectedRestaurant;
        if (!r.getImages().isEmpty())
            restaurantImage.setImage(r.getImages().get(0).getImage());
        else
            restaurantImage.setImage(DB.NO_IMAGE.getImage());
        restaurantTitle.setText(r.getTitle() + (!(r.getGrades().isEmpty()) ? " [" + r.getAverageGrade() + "]" : ""));
        restaurantDescription.setText(r.getDescription());
        restaurantStatus.setText(getRestaurantStatus(r));
        restaurantWorkdayHours.setText(r.getWorkdayTime());
        restaurantSaturdayHours.setText(r.getSaturdayTime());
        restaurantSundayHours.setText(r.getSundayTime());
    }

    @FXML
    void menuClicked() {
        Main.Locations location = DB.isLoggedIn() ? Main.Locations.REGISTERED_MEAL_MENU : Main.Locations.UNREGISTERED_MEAL_MENU;
        Main.switchScene(location);
    }

    @FXML
    void impressionsClicked() {
        Main.Locations location = DB.isLoggedIn() ? Main.Locations.REGISTERED_MEAL_IMPRESSIONS : Main.Locations.UNREGISTERED_MEAL_IMPRESSIONS;
        Main.switchScene(location);
    }

    @FXML
    void contactClicked() {
        Main.Locations location = DB.isLoggedIn() ? Main.Locations.REGISTERED_MEAL_CONTACT : Main.Locations.UNREGISTERED_MEAL_CONTACT;
        Main.switchScene(location);
    }

    @FXML
    void loginClicked() {
        Main.switchScene(Main.Locations.UNREGISTERED_LOGIN);
    }

    @FXML
    void registerClicked() {
        Main.switchScene(Main.Locations.UNREGISTERED_REGISTER);
    }

    @FXML
    void browseClicked() {
        Main.switchScene(Main.Locations.REGISTERED_BROWSE);
    }

    @FXML
    void checkoutClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHECKOUT);
    }

    @FXML
    void ordersClicked() {
        Main.switchScene(Main.Locations.REGISTERED_ORDERS);
    }

    @FXML
    void changeDataClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHANGEDATA);
    }

    @FXML
    void changePassClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHANGEPASS);
    }

    @FXML
    void logoutClicked() {
        DB.logOff();
        Main.switchScene(Main.Locations.UNREGISTERED_LOGIN);
    }

    @FXML
    void backClicked() {
        Main.Locations location = DB.isLoggedIn() ? Main.Locations.REGISTERED_BROWSE : Main.Locations.UNREGISTERED_BROWSE;
        Main.switchScene(location);
    }

}
