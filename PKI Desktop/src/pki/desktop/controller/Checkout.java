package pki.desktop.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.FlowPane;
import pki.desktop.Main;
import pki.desktop.NotificationLibrary;
import pki.desktop.controller.components.CheckoutVisual;
import pki.desktop.controller.components.PopupInput;
import pki.desktop.model.DB;
import pki.desktop.model.Order;
import pki.desktop.model.OrderedMeal;
import pki.desktop.model.Restaurant;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;

public class Checkout implements Initializable {

    @FXML
    private FlowPane pane;
    @FXML
    private Label price;
    @FXML
    private TextArea message;
    @FXML
    private Label deliveryLocation;


    private void calculatePrice() {
        List<OrderedMeal> orderedMeals = DB.checkout;
        price.setText("Vaš ukupni račun je: " + OrderedMeal.getMealPrice(orderedMeals) + " RSD");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pane.setHgap(20);
        pane.setVgap(20);
        List<OrderedMeal> orderedMeals = DB.checkout;
        for (OrderedMeal m : orderedMeals) {

            try {
                CheckoutVisual v = new CheckoutVisual(m, this);
                pane.getChildren().add(v);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        calculatePrice();
        deliveryLocation.setText("Lokacija dostave: " + DB.loggedOn.getAddress());
    }


    @FXML
    public void locationChange() {
        PopupInput popupInput = new PopupInput(pane.getScene().getWindow(), deliveryLocation);
        popupInput.togglePopup();
    }

    private String getAddress() {
        String text = new String(deliveryLocation.getText().trim());
        text = text.substring("Lokacija dostave: ".length());
        return text;
    }

    public void update(CheckoutVisual v) {
        pane.getChildren().remove(v);
        DB.checkout.remove(v.getMeal());
        calculatePrice();
    }

    @FXML
    public void order() {

        if (DB.checkout.isEmpty()) {
            NotificationLibrary.checkoutEmpty().showError();
            return;
        }

        Map<Restaurant, List<OrderedMeal>> restaurantMap = new HashMap<>();
        for (OrderedMeal meal : DB.checkout) {
            Restaurant r = meal.getMeal().getContainingRestaurant();
            if (restaurantMap.containsKey(r))
                restaurantMap.get(r).add(meal);
            else {
                List<OrderedMeal> mealList = new ArrayList<>();
                mealList.add(meal);
                restaurantMap.put(r, mealList);
            }
        }

        for (Restaurant key : restaurantMap.keySet()) {
            Order o = new Order(DB.loggedOn, key, LocalDateTime.now(), message.getText(), LocalDateTime.MIN, LocalDateTime.MIN);
            o.setMeals(restaurantMap.get(key));
            o.setAddress(getAddress());
            DB.orders.add(o);
        }
        DB.checkout.clear();
        if (restaurantMap.keySet().size() == 1)
            NotificationLibrary.orderMade().showConfirm();
        else
            NotificationLibrary.ordersMade().showConfirm();
        Main.switchScene(Main.Locations.REGISTERED_ORDERS);
    }

    @FXML
    void browseClicked() {
        Main.switchScene(Main.Locations.REGISTERED_BROWSE);
    }

    @FXML
    void checkoutClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHECKOUT);
    }

    @FXML
    void ordersClicked() {
        Main.switchScene(Main.Locations.REGISTERED_ORDERS);
    }

    @FXML
    void changeDataClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHANGEDATA);
    }

    @FXML
    void changePassClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHANGEPASS);
    }

    @FXML
    void logoutClicked() {
        DB.logOff();
        Main.switchScene(Main.Locations.UNREGISTERED_LOGIN);
    }
}

