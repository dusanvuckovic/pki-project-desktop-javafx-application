package pki.desktop.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import pki.desktop.Main;
import pki.desktop.NotificationLibrary;
import pki.desktop.model.DB;
import pki.desktop.model.User;

import java.net.URL;
import java.util.ResourceBundle;

public class Login implements Initializable {

    @FXML
    TextField usernameF;
    @FXML
    PasswordField passwordF;
    @FXML
    Label errorText;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    void registerClicked() {
        Main.switchScene(Main.Locations.UNREGISTERED_REGISTER);
    }

    @FXML
    void overviewClicked() {
        Main.switchScene(Main.Locations.UNREGISTERED_BROWSE);
    }

    @FXML
    public void login() {
        String username = usernameF.getText(), password = passwordF.getText();
        errorText.setText("");
        if ("".equals(username) || "".equals(password)) {
            NotificationLibrary.notFilled().showWarning();
            return;
        }

        if (DB.users.containsKey(username)) {
            User user = DB.users.get(username);
            if (user.getPassword().equals(password)) {
                DB.loggedOn = user;
                Main.switchScene(Main.Locations.REGISTERED_BROWSE);
            } else {
                NotificationLibrary.badPassword().showWarning();
            }

        } else {
            NotificationLibrary.badUser().showWarning();
        }

    }
}
