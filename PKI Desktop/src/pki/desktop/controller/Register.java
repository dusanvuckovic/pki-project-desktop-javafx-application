package pki.desktop.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import pki.desktop.Main;
import pki.desktop.NotificationLibrary;
import pki.desktop.model.DB;
import pki.desktop.model.User;

import java.net.URL;
import java.util.ResourceBundle;

public class Register implements Initializable {

    private static final String ERR_FIELDS = "Morate uneti sva polja!",
            ERR_EXISTING = "vec postoji",
            ERR_DIFF_PASS = "razlicite lozinke!";
    @FXML
    TextField usernameF, firstNameF, lastNameF, phoneF, addressF;
    @FXML
    PasswordField passwordF, reptPasswordF;
    @FXML
    Label errorText;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    void loginClicked() {
        Main.switchScene(Main.Locations.UNREGISTERED_LOGIN);
    }

    @FXML
    void overviewClicked() {
        Main.switchScene(Main.Locations.UNREGISTERED_BROWSE);
    }

    private void checkForErrors() {
        errorText.setText("");
        if (usernameF.getText().trim().isEmpty() || firstNameF.getText().trim().isEmpty()
                || lastNameF.getText().trim().isEmpty() || phoneF.getText().trim().isEmpty()
                || addressF.getText().trim().isEmpty() || passwordF.getText().trim().isEmpty()
                || reptPasswordF.getText().trim().isEmpty()) {
            NotificationLibrary.notFilled().showWarning();
        } else if (DB.users.containsKey(usernameF.getText())) {
            NotificationLibrary.userExisting().showWarning();
        } else if (!reptPasswordF.getText().equals(passwordF.getText()))
            NotificationLibrary.diffPass().showWarning();
    }

    @FXML
    void register() {
        checkForErrors();
        if ("".equals(errorText.getText())) {
            User u = new User(firstNameF.getText(), lastNameF.getText(), phoneF.getText(), addressF.getText(), usernameF.getText(), passwordF.getText());
            DB.users.put(u.getUsername(), u);
            DB.loggedOn = u;
            Main.switchScene(Main.Locations.REGISTERED_BROWSE);
        }
    }
}