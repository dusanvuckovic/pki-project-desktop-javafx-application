package pki.desktop.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import pki.desktop.model.DB;
import pki.desktop.model.Restaurant;

import java.net.URL;
import java.util.ResourceBundle;

public class MealContact extends MealBase implements Initializable {


    @FXML
    private Label contact;
    @FXML
    private Label address;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeStatus();

        Restaurant r = DB.selectedRestaurant;
        //contact.setText(r.get);
        StringBuilder adr = new StringBuilder("Kontakt:");
        for (String s: r.getPhones())
            adr.append("\n").append(s);
        contact.setText(adr.toString());
    }

}
