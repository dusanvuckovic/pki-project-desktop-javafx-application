package pki.desktop.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;
import pki.desktop.Main;
import pki.desktop.controller.components.OrderVisual;
import pki.desktop.model.DB;
import pki.desktop.model.Order;

import java.net.URL;
import java.util.ResourceBundle;

public class Orders implements Initializable {

    @FXML
    private VBox box;

    @FXML
    void browseClicked() {
        Main.switchScene(Main.Locations.REGISTERED_BROWSE);
    }

    @FXML
    void checkoutClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHECKOUT);
    }

    @FXML
    void ordersClicked() {
        Main.switchScene(Main.Locations.REGISTERED_ORDERS);
    }

    @FXML
    void changeDataClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHANGEDATA);
    }

    @FXML
    void changePassClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHANGEPASS);
    }

    @FXML
    void logoutClicked() {
        DB.logOff();
        Main.switchScene(Main.Locations.UNREGISTERED_LOGIN);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            for (Order o : DB.orders)
                if (o.getUserWhoOrdered() == DB.loggedOn)
                    box.getChildren().add(new OrderVisual(o));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
