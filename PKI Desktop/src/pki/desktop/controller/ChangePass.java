package pki.desktop.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import pki.desktop.Main;
import pki.desktop.NotificationLibrary;
import pki.desktop.model.DB;


public class ChangePass {

    @FXML
    Label errLabel;
    @FXML
    PasswordField oldPasswordF, newPasswordF, reptPasswordF;


    @FXML
    void browseClicked() {
        Main.switchScene(Main.Locations.REGISTERED_BROWSE);
    }

    @FXML
    void checkoutClicked() {
        Main.switchScene(Main.Locations.REGISTERED_ORDERS);
    }

    @FXML
    void ordersClicked() {
        Main.switchScene(Main.Locations.REGISTERED_ORDERS);
    }

    @FXML
    void changeDataClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHANGEDATA);
    }

    @FXML
    void changePassClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHANGEPASS);
    }

    @FXML
    void logoutClicked() {
        DB.logOff();
        Main.switchScene(Main.Locations.UNREGISTERED_LOGIN);
    }

    @FXML
    void changePassword() {
        String oldPass = oldPasswordF.getText(), newPass = newPasswordF.getText(), reptPass = reptPasswordF.getText();

        errLabel.setText("");
        if (oldPass.trim().isEmpty() || newPass.trim().isEmpty() || reptPass.trim().isEmpty())
            NotificationLibrary.notFilled().showWarning();
        else if (!oldPass.equals(DB.loggedOn.getPassword()))
            NotificationLibrary.badPassword().showWarning();
        else if (!newPass.equals(reptPass))
            NotificationLibrary.diffPass().showWarning();
        else {
            DB.loggedOn.setPassword(newPass);
            NotificationLibrary.changedPass().showInformation();
            Main.switchScene(Main.Locations.REGISTERED_BROWSE);
        }
    }


}
