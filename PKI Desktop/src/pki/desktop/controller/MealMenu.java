package pki.desktop.controller;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import org.controlsfx.glyphfont.Glyph;
import pki.desktop.NotificationLibrary;
import pki.desktop.controller.components.CommentVisual;
import pki.desktop.model.*;

import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;

@SuppressWarnings("ALL")
public class MealMenu extends MealBase implements Initializable {


    @FXML
    TableView<Meal> mealsTableView;
    @FXML
    private TableColumn<Meal, String> nameColumn;
    @FXML
    private TableColumn<Meal, String> typeColumn;
    @FXML
    private TableColumn<Meal, Integer> priceColumn;
    @FXML
    private TableColumn<Meal, String> ingredientsColumn;
    @FXML
    private TableColumn<Meal, Integer> commentsColumn;
    @FXML
    private VBox buttonBoxLeft;
    @FXML
    private VBox buttonBoxRight;


    @FXML
    private TitledPane mealPane;
    @FXML
    private Label mealName;
    @FXML
    private Label mealPrice;
    @FXML
    private Label mealDescription;
    @FXML
    private TextField mealAmount;
    @FXML
    private Label labelDefault;
    @FXML
    private ImageView mealPhoto;

    @FXML
    private FlowPane commentPane;
    @FXML
    private TextArea commentTextArea;
    @FXML
    private Button commentButton;
    @FXML
    private Button orderButton;

    @FXML
    private Glyph mealShoppingCart;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeStatus();
        initializeTable();
        initializeMealPane();

        if (!DB.isLoggedIn()) {
            orderButton.setDisable(true);
            commentButton.setDisable(true);
            //commentTextArea.setDisable(true);
            mealShoppingCart.setDisable(true);
            mealAmount.setDisable(true);
        } else {
            commentButton.disableProperty().bind(Bindings.isEmpty(commentTextArea.textProperty()));
        }
    }


    private void initializeTable() {
        Restaurant r = DB.selectedRestaurant;
        mealsTableView.setItems(FXCollections.observableList(r.getMeals()));
        nameColumn.setCellValueFactory(new PropertyValueFactory("name"));
        typeColumn.setCellValueFactory(new PropertyValueFactory("fullTypeName"));
        priceColumn.setCellValueFactory(new PropertyValueFactory("price"));
        ingredientsColumn.setCellValueFactory(p -> {
            StringBuilder sb = new StringBuilder();
            for (String s : p.getValue().getIngredients()) {
                sb.append(s);
                sb.append(", ");
            }
            String s = sb.substring(0, sb.length() - 2);
            return new SimpleStringProperty(s);
        });
        commentsColumn.setCellValueFactory(p -> new SimpleIntegerProperty(p.getValue().getComments().size()).asObject());
    }

    private void initializeMealPane() {

        mealPane.setVisible(false);

        mealPane.expandedProperty().addListener(this::mealPaneChange);
        mealAmount.setTextFormatter(new TextFormatter<Integer>(this::mealAmountChange));

    }

    private void openMealPane() {
        buttonBoxLeft.setDisable(true);
        buttonBoxRight.setDisable(true);
        mealPane.setVisible(true);
        mealPane.setExpanded(true);
    }

    private void closeMealPane() {
        buttonBoxLeft.setDisable(false);
        buttonBoxRight.setDisable(false);
        mealPane.setVisible(false);
        mealPane.setExpanded(true);
    }

    private void mealPaneChange(ObservableValue<?> a, Boolean wasExpanded, Boolean isExpanded) {
        closeMealPane();
    }

    private TextFormatter.Change mealAmountChange(TextFormatter.Change change) {
        if (change.isReplaced() && change.getText().matches("[^0-9]]"))
            return null;
        if (change.isAdded() && change.getText().matches("[^0-9]"))
            return null;
        if (change.getControlNewText().length() > 3)
            return null;
        return change;
    }

    @FXML
    void orderPressed() {
        int number = Integer.parseInt(mealAmount.getText());
        if (number < 1) {
            NotificationLibrary.badNumber().showError();
            return;
        }
        DB.checkout.add(new OrderedMeal(DB.selectedMeal, Integer.parseInt(mealAmount.getText())));
        NotificationLibrary.mealAdded().showConfirm();
        closeMealPane();
    }

    @FXML
    void selectTable() {
        if (mealsTableView.getSelectionModel().getSelectedItem() == null)
            return;
        Meal m = DB.selectedMeal = mealsTableView.getSelectionModel().getSelectedItem();
        mealName.setText(m.getName() + " (" + DB.selectedRestaurant.getTitle() + ")");
        mealDescription.setText(m.getDescription());
        mealPrice.setText(Integer.toString(m.getPrice()) + " RSD");
        mealAmount.setText("1");
        mealPhoto.setImage(m.getImages().isEmpty() ? DB.NO_IMAGE.getImage() : m.getImages().get(0));


        Collections.sort(DB.selectedMeal.getComments());
        commentPane.getChildren().clear();
        if (DB.selectedMeal.getComments().isEmpty())
            commentPane.getChildren().add(labelDefault);
        else
            for (Comment c : DB.selectedMeal.getComments())
                commentPane.getChildren().addAll(new CommentVisual(c));

        openMealPane();
    }

    @FXML
    void commentMealClicked() {
        String text = commentTextArea.getText().trim();
        if (text.isEmpty())
            return;
        commentTextArea.clear();
        Comment c = new Comment(text, DB.loggedOn, DB.getDateTime());
        DB.selectedMeal.getComments().add(c);
        Collections.sort(DB.selectedMeal.getComments());
        commentPane.getChildren().add(0, new CommentVisual(c));
        commentPane.getChildren().remove(labelDefault);
    }

}
