package pki.desktop.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.util.StringConverter;
import org.controlsfx.control.PrefixSelectionChoiceBox;
import pki.desktop.Main;
import pki.desktop.model.Category;
import pki.desktop.model.DB;
import pki.desktop.model.Restaurant;
import pki.desktop.model.Type;

import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class Overview implements Initializable {

    @FXML
    TableView<Restaurant> restaurantTableView;

    @FXML
    private TableColumn<Restaurant, String> names;
    @FXML
    private TableColumn<Restaurant, String> addresses;
    @FXML
    private TableColumn<Restaurant, String> categories;
    @FXML
    private TableColumn<Restaurant, String> deliveryTime;
    @FXML
    private TableColumn<Restaurant, Integer> minimumPrice;
    @FXML
    private TableColumn<Restaurant, String> openUntil;


    @FXML
    private AnchorPane searchPane;
    @FXML
    private TextField searchRestaurant;
    @FXML
    private TextField searchMeal;
    @FXML
    private PrefixSelectionChoiceBox searchCategory;
    @FXML
    private PrefixSelectionChoiceBox searchType;

    private final ChangeListener searchListener = (ob, o, n) -> search();


    private Restaurant.Hours getCurrentHours(Restaurant r) {
        DayOfWeek dow = DB.getDayOfWeek();
        int index = 0;
        if (dow == DayOfWeek.SATURDAY)
            index = 1;
        else if (dow == DayOfWeek.SUNDAY)
            index = 2;
        return r.getHours()[index];
    }

    private String getOpenNotification(Restaurant r) {
        Restaurant.Hours hours = getCurrentHours(r);
        LocalTime timeNow = DB.getTime();

        if (!hours.works) {
            return "Danas ne radi";
        } else if (timeNow.isBefore(hours.start))
            return "Otvara se u " + hours.start.format(DateTimeFormatter.ofPattern("HH:ss"));
        else if (timeNow.isAfter(hours.end))
            return "Zatvorilo se u " + hours.end.format(DateTimeFormatter.ofPattern("HH:ss"));
        else
            return "Radi do " + hours.end.format(DateTimeFormatter.ofPattern("HH:ss"));

    }

    private void initializeTable() {
        restaurantTableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList<Restaurant> restaurants = FXCollections.observableList(DB.restaurants);
        restaurantTableView.setItems(restaurants);
        names.setCellValueFactory(new PropertyValueFactory("title"));
        addresses.setCellValueFactory(new PropertyValueFactory("address"));
        categories.setCellValueFactory(new PropertyValueFactory("categoryFullName"));
        minimumPrice.setCellValueFactory(new PropertyValueFactory("minPrice"));
        deliveryTime.setCellValueFactory(new PropertyValueFactory("deliveryTime"));
        openUntil.setCellValueFactory(p -> {
                    Restaurant r = p.getValue();
                    return new SimpleStringProperty(getOpenNotification(r));
                }
        );
    }

    private void initializeSearch() {
        searchCategory.setItems(FXCollections.observableList(Arrays.asList(Category.values())));
        searchCategory.setConverter(new StringConverter() {
            @Override
            public String toString(Object object) {
                if (object == Category.NONE)
                    return "Sve kategorije";
                else
                    return ((Category) object).getFullName();
            }

            @Override
            public Object fromString(String enumName) {
                return Category.valueOf(enumName);
            }
        });
        searchCategory.getSelectionModel().select(0);

        searchType.setItems(FXCollections.observableList(Arrays.asList(Type.values())));
        searchType.setConverter(new StringConverter() {
            @Override
            public String toString(Object object) {
                if (object == Type.NONE)
                    return "Svi tipovi";
                else
                    return ((Type)object).getFullName();
            }

            @Override
            public Object fromString(String enumName) {
                return Type.valueOf(enumName);
            }
        });
        searchType.getSelectionModel().select(0);

        searchRestaurant.textProperty().addListener(searchListener);
        searchMeal.textProperty().addListener(searchListener);
        searchCategory.getSelectionModel().selectedIndexProperty().addListener(searchListener);
        searchType.getSelectionModel().selectedIndexProperty().addListener(searchListener);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeTable();
        initializeSearch();
    }

    @FXML
    void selectTable() {
        if (restaurantTableView.getSelectionModel().getSelectedItem() == null)
            return;
        DB.selectedRestaurant = restaurantTableView.getSelectionModel().getSelectedItem();
        Main.Locations location = DB.isLoggedIn() ? Main.Locations.REGISTERED_MEAL_MENU : Main.Locations.UNREGISTERED_MEAL_MENU;
        Main.switchScene(location);
    }

    private void search() {
        String searchTitle = searchRestaurant.getText();
        String searchMealName = searchMeal.getText();
        Category searchCategoryO = Category.values()
                [searchCategory.getSelectionModel().getSelectedIndex()];
        Type searchTypeO = Type.values()[searchType.getSelectionModel().getSelectedIndex()];
        List<Restaurant> l = new ArrayList<>(DB.restaurants);
        l.removeIf(r -> !r.searchRestaurantTitle(searchTitle));
        l.removeIf(r -> !r.searchMealName(searchMealName));
        l.removeIf(r -> !r.searchCategory(searchCategoryO));
        l.removeIf(r -> !r.searchType(searchTypeO));
        restaurantTableView.setItems(FXCollections.observableList(l));
    }

    @FXML
    void browseClicked() {
        Main.switchScene(Main.Locations.REGISTERED_BROWSE);
    }

    @FXML
    void checkoutClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHECKOUT);
    }

    @FXML
    void ordersClicked() {
        Main.switchScene(Main.Locations.REGISTERED_ORDERS);
    }

    @FXML
    void changeDataClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHANGEDATA);
    }

    @FXML
    void changePassClicked() {
        Main.switchScene(Main.Locations.REGISTERED_CHANGEPASS);
    }

    @FXML
    void loginClicked() {
        Main.switchScene(Main.Locations.UNREGISTERED_LOGIN);
    }

    @FXML
    void registerClicked() {
        Main.switchScene(Main.Locations.UNREGISTERED_REGISTER);
    }

    @FXML
    void logoutClicked() {
        DB.logOff();
        Main.switchScene(Main.Locations.UNREGISTERED_LOGIN);
    }


}
