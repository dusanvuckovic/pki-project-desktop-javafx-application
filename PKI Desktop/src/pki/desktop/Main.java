package pki.desktop;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pki.desktop.model.DB;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class Main extends Application {

    private static final Map<Locations, URL> LOCATIONS_TO_URLS = new HashMap<>();
    private static Stage primaryStage;

    public static void switchScene(Locations location) {
        try {
            Parent root = FXMLLoader.load(LOCATIONS_TO_URLS.get(location));
            primaryStage.setScene(new Scene(root, 1200, 900));
        } catch (IOException e) {
            System.out.println("Greška pri promeni lokacije na " + location.toString());
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void initializeSceneURLs() {
        LOCATIONS_TO_URLS.put(Locations.UNREGISTERED_LOGIN, getClass().getResource("/pki/desktop/view/unregistered/" + "login" + ".fxml"));
        LOCATIONS_TO_URLS.put(Locations.UNREGISTERED_REGISTER, getClass().getResource("/pki/desktop/view/unregistered/" + "register" + ".fxml"));
        LOCATIONS_TO_URLS.put(Locations.UNREGISTERED_BROWSE, getClass().getResource("/pki/desktop/view/unregistered/" + "overview" + ".fxml"));

        LOCATIONS_TO_URLS.put(Locations.REGISTERED_BROWSE, getClass().getResource("/pki/desktop/view/registered/" + "overview" + ".fxml"));
        LOCATIONS_TO_URLS.put(Locations.REGISTERED_CHECKOUT, getClass().getResource("/pki/desktop/view/registered/" + "checkout" + ".fxml"));
        LOCATIONS_TO_URLS.put(Locations.REGISTERED_ORDERS, getClass().getResource("/pki/desktop/view/registered/" + "orders" + ".fxml"));
        LOCATIONS_TO_URLS.put(Locations.REGISTERED_CHANGEDATA, getClass().getResource("/pki/desktop/view/registered/" + "changedata" + ".fxml"));
        LOCATIONS_TO_URLS.put(Locations.REGISTERED_CHANGEPASS, getClass().getResource("/pki/desktop/view/registered/" + "changepass" + ".fxml"));

        LOCATIONS_TO_URLS.put(Locations.REGISTERED_MEAL_MENU, getClass().getResource("/pki/desktop/view/registered/" + "meal01menu" + ".fxml"));
        LOCATIONS_TO_URLS.put(Locations.REGISTERED_MEAL_IMPRESSIONS, getClass().getResource("/pki/desktop/view/registered/" + "meal02impressions" + ".fxml"));
        LOCATIONS_TO_URLS.put(Locations.REGISTERED_MEAL_CONTACT, getClass().getResource("/pki/desktop/view/registered/" + "meal03contact" + ".fxml"));

        LOCATIONS_TO_URLS.put(Locations.UNREGISTERED_MEAL_MENU, getClass().getResource("/pki/desktop/view/unregistered/" + "meal01menu" + ".fxml"));
        LOCATIONS_TO_URLS.put(Locations.UNREGISTERED_MEAL_IMPRESSIONS, getClass().getResource("/pki/desktop/view/unregistered/" + "meal02impressions" + ".fxml"));
        LOCATIONS_TO_URLS.put(Locations.UNREGISTERED_MEAL_CONTACT, getClass().getResource("/pki/desktop/view/unregistered/" + "meal03contact" + ".fxml"));

    }

    @Override
    public void start(Stage primaryStage) {
        initializeSceneURLs();
        Main.primaryStage = primaryStage;

        primaryStage.setTitle("Naručivanje hrane");
        primaryStage.setResizable(false);
        DB.loggedOn = DB.users.get("mi");

        switchScene(Locations.REGISTERED_BROWSE);
        primaryStage.show();
    }

    public enum Locations {
        UNREGISTERED_LOGIN,
        UNREGISTERED_REGISTER,
        UNREGISTERED_BROWSE,
        UNREGISTERED_MEAL_MENU,
        UNREGISTERED_MEAL_IMPRESSIONS,
        UNREGISTERED_MEAL_CONTACT,

        REGISTERED_BROWSE,
        REGISTERED_CHECKOUT,
        REGISTERED_ORDERS,
        REGISTERED_CHANGEDATA,
        REGISTERED_CHANGEPASS,
        REGISTERED_MEAL_MENU,
        REGISTERED_MEAL_IMPRESSIONS,
        REGISTERED_MEAL_CONTACT

    }
}
