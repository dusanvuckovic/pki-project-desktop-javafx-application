package pki.desktop;

import javafx.geometry.Pos;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

public class NotificationLibrary {

    private static final String
            NOT_FILLED = "Morate popuniti sva polja!",
            BAD_PASS = "Pogrešna lozinka!",
            BAD_USER = "Takvo korisničko ime ne postoji!",
            USER_EXISTING = "Već postoji korisnik sa takvim registarskim imenom!",
            DIFF_PASS = "Lozinke se ne poklapaju!",
            BAD_NUMBER = "Loše unet broj porcija!",
            SUCCESS_ADDED = "Hrana dodata u korpu!",
            SUCCESS_ORDERED = "Hrana naručena!",
            CHECKOUT_EMPTY = "Korpa je prazna!",
            SUCCESS_ORDERED_M = "Hrana naručena iz više restorana!",
            SUCCESS_DATA_CHANGED = "Uspešno promenjeni podaci!",
            SUCCESS_PASS_CHANGED = "Uspešno promenjena lozinka!";

    private static Notifications makeNotification() {
        return Notifications.create()
                .hideAfter(Duration.seconds(1.5f))
                .position(Pos.CENTER)
                .title("Obaveštenje: ");

    }

    private static Notifications makeNotification(String text) {
        return makeNotification().text(text);
    }

    public static Notifications notFilled() {
        return makeNotification(NOT_FILLED);
    }
    public static Notifications badPassword() {
        return makeNotification(BAD_PASS);
    }
    public static Notifications badUser() {
        return makeNotification(BAD_USER);
    }
    public static Notifications userExisting() {
        return makeNotification(USER_EXISTING);
    }
    public static Notifications diffPass() {
        return makeNotification(DIFF_PASS);
    }
    public static Notifications mealAdded() {
        return makeNotification(SUCCESS_ADDED);
    }
    public static Notifications orderMade() {
        return makeNotification(SUCCESS_ORDERED);
    }
    public static Notifications ordersMade() {
        return makeNotification(SUCCESS_ORDERED_M);
    }
    public static Notifications changedData() {
        return makeNotification(SUCCESS_DATA_CHANGED);
    }
    public static Notifications changedPass() {
        return makeNotification(SUCCESS_PASS_CHANGED);
    }
    public static Notifications badNumber() {
        return makeNotification(BAD_NUMBER);
    }
    public static Notifications checkoutEmpty() {
        return makeNotification(CHECKOUT_EMPTY);
    }

}
