package pki.desktop.model;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class Order {

    private static int globalID = 0;

    private int ID;
    private List<OrderedMeal> meals;
    private User userWhoOrdered;
    private Restaurant restaurantDelivering;
    private LocalDateTime orderedOn;
    private String address;
    private String messageToDeliverer;
    private Status status;
    private LocalDateTime acceptedOn;
    private LocalDateTime deliveredOrDeniedOn;

    public Order(User userWhoOrdered, Restaurant restaurantDelivering, LocalDateTime orderedOn, String messageToDeliverer, LocalDateTime acceptedOn, LocalDateTime deliveredOrDeniedOn) {
        this.ID = globalID++;
        this.userWhoOrdered = userWhoOrdered;
        this.meals = new LinkedList<>();
        this.restaurantDelivering = restaurantDelivering;
        this.orderedOn = orderedOn;
        this.address = userWhoOrdered.getAddress();
        this.messageToDeliverer = messageToDeliverer;
        this.acceptedOn = acceptedOn;
        this.deliveredOrDeniedOn = deliveredOrDeniedOn;
        this.status = Status.PENDING;
    }

    public Order(User userWhoOrdered, Restaurant restaurantDelivering, LocalDateTime orderedOn, String messageToDeliverer, LocalDateTime acceptedOn, LocalDateTime deliveredOrDeniedOn, Status status) {
        this(userWhoOrdered, restaurantDelivering, orderedOn, messageToDeliverer, acceptedOn, deliveredOrDeniedOn);
        this.status = status;
    }

    public String getDescriptionFromStatus() {
        switch (status) {
            case PENDING:
                return "Dostavlja se!";
            case DELIVERED:
                return "Preuzeta!";
            case FAILED:
                return "Obustavljena!";
        }
        return "";
    }

    public int calculatePrice() {
        int price = 0;
        for (OrderedMeal o : meals) {
            price += o.getHowMany() * o.getMeal().getPrice();
        }
        return price;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public List<OrderedMeal> getMeals() {
        return meals;
    }

    public void setMeals(List<OrderedMeal> meals) {
        this.meals = meals;
    }

    public User getUserWhoOrdered() {
        return userWhoOrdered;
    }

    public void setUserWhoOrdered(User userWhoOrdered) {
        this.userWhoOrdered = userWhoOrdered;
    }

    public Restaurant getRestaurantDelivering() {
        return restaurantDelivering;
    }

    public void setRestaurantDelivering(Restaurant restaurantDelivering) {
        this.restaurantDelivering = restaurantDelivering;
    }

    public LocalDateTime getOrderedOn() {
        return orderedOn;
    }

    public void setOrderedOn(LocalDateTime orderedOn) {
        this.orderedOn = orderedOn;
    }

    public String getMessageToDeliverer() {
        return messageToDeliverer;
    }

    public void setMessageToDeliverer(String messageToDeliverer) {
        this.messageToDeliverer = messageToDeliverer;
    }

    public LocalDateTime getAcceptedOn() {
        return acceptedOn;
    }

    public void setAcceptedOn(LocalDateTime acceptedOn) {
        this.acceptedOn = acceptedOn;
    }

    public LocalDateTime getDeliveredOrDeniedOn() {
        return deliveredOrDeniedOn;
    }

    public void setDeliveredOrDeniedOn(LocalDateTime deliveredOrDeniedOn) {
        this.deliveredOrDeniedOn = deliveredOrDeniedOn;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        return ID == order.ID;
    }

    @Override
    public int hashCode() {
        return ID;
    }

    public enum Status {
        PENDING,
        DELIVERED,
        FAILED
    }
}
