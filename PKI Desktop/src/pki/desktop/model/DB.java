package pki.desktop.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.*;

public class DB {

    public static final ImageView NO_IMAGE = new ImageView(new Image("/pki/desktop/data/noimage.jpg"));
    public static final List<Restaurant> restaurants = new LinkedList<Restaurant>();
    public static final List<Order> orders = new LinkedList<>();
    public static final Map<String, User> users = new HashMap<String, User>();
    public static final List<OrderedMeal> checkout = new LinkedList<>();
    private static final User NOT_LOGGED_ON = new User("", "", "", "", "", "");
    private static final Restaurant NO_RESTAURANT = new Restaurant("", "", Category.NONE, "", 0, 0);
    private static final Meal NO_MEAL = new Meal("", Category.NONE, Type.NONE, "", 0, NO_RESTAURANT);
    public static User loggedOn = NOT_LOGGED_ON;
    public static Restaurant selectedRestaurant = NO_RESTAURANT;
    public static Meal selectedMeal = NO_MEAL;

    static {
        User u = new User("Miloš", "Petrović", "065456789", "Kralja Petra 13/2", "mi", "im");
        users.put(u.getUsername(), u);

        /* BOTAKO */
        Restaurant botako = new Restaurant(
                "Botako",
                "Nevesinjska 6",
                Category.ITALIAN_FOOD,
                "Botako od 2004. godine spaja tradiciju " +
                        "italijanske kuhinje sa domaćom tradicijom. Uz autentičan ambijent, na Botako meniu " +
                        "se nalaze italijanske pice, lazanje, paste, poslastice, brusketi i salate, kao i " +
                        "nekoliko varijacija doručka. U novembru 2013. je otvoren još jedan Botako restoran, " +
                        "na atraktivnoj lokaciji - blizu centra - Šantićeva br.8.",
                1000,
                45
        );
        botako.addHours(
                Restaurant.Day.WORKDAY,
                LocalTime.of(10, 0),
                LocalTime.of(20, 30)
        );
        botako.addHours(
                Restaurant.Day.SATURDAY,
                LocalTime.of(11, 0),
                LocalTime.of(22, 30)
        );
        botako.addHours(
                Restaurant.Day.SUNDAY,
                LocalTime.of(9, 0),
                LocalTime.of(23, 0)
        );
        Collections.addAll(botako.getGrades(), 5, 5, 4, 5);
        Collections.addAll(botako.getPhones(), "061232568", "0649193493");
        Collections.addAll(
                botako.getComments(),
                new Comment(
                        "Divan restoran!",
                        "joca",
                        LocalDateTime.of(2017, Month.DECEMBER, 21, 21, 12, 43)
                ),
                new Comment(
                        "Sjajna muzika!",
                        "ana91",
                        LocalDateTime.of(2017, Month.DECEMBER, 15, 4, 2, 5))
        );
        botako.getGrades().addAll(Arrays.asList(5, 5, 4, 3, 5, 5));

        Meal vesuvio = new Meal(
                "Vesuvio",
                Category.ITALIAN_FOOD,
                Type.PIZZA,
                "Klasična pica sa šunkom",
                800,
                botako
        );
        vesuvio.getImages().add(new Image("/pki/desktop/data/botakovesuvio.jpg"));

        Collections.addAll(vesuvio.getIngredients(),
                "pelat", "sir", "praška šunka", "svež paradajz", "masline", "origano", "susam");
        vesuvio.getComments().addAll(Arrays.asList(
                new Comment(
                        "Vrlo ukusno!",
                        "boki43",
                        LocalDateTime.of(2017, Month.DECEMBER, 21, 21, 12, 43)
                ),
                new Comment(
                        "Neprofesionalno. Čekao sam više od sat vremena i na kraju je pica stigla hladna. Oprez.",
                        "MilosJovanovic",
                        LocalDateTime.of(2017, Month.JANUARY, 11, 23, 11, 23)
                ),
                new Comment(
                        "nije lose samo dve masline",
                        "JohnMcClane",
                        LocalDateTime.of(2017, Month.SEPTEMBER, 1, 15, 22, 15)),
                new Comment(
                        "ok, ne stede na salami za razliku od mnogih, narucio bih ponovo",
                        "jovan_polje",
                        LocalDateTime.of(2017, Month.JULY, 4, 18, 1, 12)
                ),
                new Comment(
                        "Testo je gletavo i nista ne valja",
                        "maja94",
                        LocalDateTime.of(2017, Month.APRIL, 1, 12, 4, 17)
                )
        ));

        Meal serbiana = new Meal(
                "Serbiana",
                Category.ITALIAN_FOOD,
                Type.PIZZA,
                "Srpska pica sa kulenom i pavlakom",
                900,
                botako
        );
        Collections.addAll(serbiana.getIngredients(),
                "pelat", "sir", "pančeta", "šampinjoni", "pavlaka", "kulen", "ljute papričice", "origano", "susam");

        Meal carbonara = new Meal(
                "Carbonara",
                Category.ITALIAN_FOOD,
                Type.PASTA,
                "Kremasta bela pasta",
                500,
                botako
        );
        Collections.addAll(
                carbonara.getIngredients(),
                "maslinovo ulje", "beli luk", "žumance", "panna", "pančeta", "šunka", "parmezan", "bosiljak", "peršun");

        Meal gambori = new Meal(
                "Gambori sos",
                Category.ITALIAN_FOOD,
                Type.PASTA,
                "Ukusni plodovi mora",
                670,
                botako
        );
        Collections.addAll(gambori.getIngredients(),
                "maslinovo ulje", "beli luk", "gambori", "paradajz sos", "peršun", "parmezan");

        Collections.addAll(botako.getMeals(), vesuvio, serbiana, carbonara, gambori);
        ImageView botakoImg = new ImageView(new Image("/pki/desktop/data/botako.jpg"));
        ImageView botakoImg2 = new ImageView(new Image("/pki/desktop/data/botako2.jpg"));
        ImageView botakoImg3 = new ImageView(new Image("/pki/desktop/data/botako3.jpg"));
        Collections.addAll(botako.getImages(), botakoImg, botakoImg2, botakoImg3);
        /* BOTAKO */

        /* LITTLE BAY */
        Restaurant littleBay = new Restaurant(
                "Little Bay",
                "Dositejeva 9",
                Category.GOURMAND,
                "Little Bay restoran je deo istoimenog lanca restorana u Londonu i Brajtonu. Posle četiri" +
                        " uspešna Little Bay restorana u Londonu njihov vlasnik Dragiša Piter Ilić rešio je " +
                        "da domovini prenese deo svog bogatog iskustva stečenog u decenijskom radu u oštroj" +
                        " konkurenciji svetske prestonice ugostiteljstva.",
                1500,
                60
        );
        littleBay.addHours(
                Restaurant.Day.WORKDAY,
                LocalTime.of(17, 0),
                LocalTime.of(23, 30)
        );
        littleBay.addHours(
                Restaurant.Day.SATURDAY,
                LocalTime.of(16, 0),
                LocalTime.of(22, 30)
        );
        littleBay.addHours(
                Restaurant.Day.SUNDAY,
                LocalTime.of(18, 0),
                LocalTime.of(23, 30)
        );
        Collections.addAll(littleBay.getGrades(), 5);
        Collections.addAll(littleBay.getPhones(), "061265568", "0642193493");

        Meal falafel = new Meal(
                "Falafel",
                Category.WORLD_FOOD,
                Type.STARTER,
                "Ukusno arapsko jelo",
                300,
                littleBay
        );
        Collections.addAll(falafel.getIngredients(),
                "leblebija", "beli luk", "tucana paprika");

        Meal penePasta = new Meal(
                "Pene pasta sa piletinom",
                Category.ITALIAN_FOOD,
                Type.PASTA,
                "Pasta sa piletinom",
                600,
                littleBay
        );
        Collections.addAll(penePasta.getIngredients(),
                "pelat", "sir", "pančeta", "šampinjoni", "pavlaka", "kulen", "ljute papričice", "origano", "susam");

        Meal duck = new Meal(
                "Pačetina u lisnatom testu",
                Category.GOURMAND,
                Type.GRILL,
                "Pečena patka",
                1500,
                littleBay
        );
        Collections.addAll(duck.getIngredients(),
                "patka", "praziluk", "crveni kupus");
        duck.getImages().add(new Image("/pki/desktop/data/littlebaypacetina.jpg"));

        Meal steak = new Meal(
                "Biftek",
                Category.GOURMAND,
                Type.GRILL,
                "Sočna t-bone poslastica",
                1880,
                littleBay
        );
        Collections.addAll(steak.getIngredients(),
                "stejk", "rukola", "čeri paradajz", "parmezan", "ulje od tartufa"
        );

        ImageView littleBayImg = new ImageView(new Image("/pki/desktop/data/littlebay.jpg"));
        ImageView littleBayImg2 = new ImageView(new Image("/pki/desktop/data/littlebay2.jpg"));
        ImageView littleBayImg3 = new ImageView(new Image("/pki/desktop/data/littlebay3.jpg"));

        Collections.addAll(littleBay.getImages(), littleBayImg, littleBayImg2, littleBayImg3);

        Collections.addAll(littleBay.getMeals(), falafel, penePasta, duck, steak);

        /* LITTLE BAY */


        Collections.addAll(restaurants, botako, littleBay);


        /*
         * narudžbine
         * */

        Order o1 = new Order(
                u,
                botako,
                LocalDateTime.of(2017, 11, 23, 23, 11, 43),
                "pas ne ujeda",
                LocalDateTime.of(2017, 11, 23, 23, 13, 32),
                LocalDateTime.of(2017, 11, 23, 23, 55, 1),
                Order.Status.DELIVERED
        );
        o1.getMeals().addAll(Arrays.asList(
                new OrderedMeal(serbiana, 1)
        ));
        Order o2 = new Order(
                u,
                botako,
                LocalDateTime.of(2017, 12, 3, 11, 22, 33),
                "pas ne ujeda",
                LocalDateTime.of(2017, 12, 3, 11, 55, 1),
                LocalDateTime.of(2017, 12, 3, 12, 30, 5),
                Order.Status.DELIVERED
        );
        o2.getMeals().addAll(Arrays.asList(
                new OrderedMeal(serbiana, 2),
                new OrderedMeal(carbonara, 1)
        ));
        Order o3 = new Order(
                u,
                littleBay,
                LocalDateTime.of(2017, 12, 9, 18, 30, 43),
                "pas ujeda",
                LocalDateTime.of(2017, 12, 9, 18, 32, 32),
                LocalDateTime.of(2017, 12, 9, 19, 15, 1)
        );
        o3.getMeals().addAll(Arrays.asList(
                new OrderedMeal(penePasta, 1),
                new OrderedMeal(steak, 1)
        ));

        orders.addAll(Arrays.asList(o1, o2, o3));

        selectedRestaurant = botako;
    }

    public static boolean isLoggedIn() {
        return loggedOn != NOT_LOGGED_ON;
    }

    public static int getCheapestMeal() {
        return restaurants.stream().map(Restaurant::getMinMealPrice).min(Integer::min).get();
    }

    public static int getPriciestMeal() {
        return restaurants.stream().map(Restaurant::getMaxMealPrice).max(Integer::max).get();
    }

    public static LocalTime getTime() {
        return LocalTime.now();
    }

    public static LocalDateTime getDateTime() {
        return LocalDateTime.now();
    }

    public static DayOfWeek getDayOfWeek() {
        return LocalDateTime.now().getDayOfWeek();
    }

    public static void logOff() {
        loggedOn = NOT_LOGGED_ON;
        DB.checkout.clear();
    }
}
