package pki.desktop.model;

import javafx.scene.image.Image;

import java.util.LinkedList;
import java.util.List;

public class Meal {

    /*•	naziva jela
•	kategorije jela – nalik na doručak/ručak/predjelo
•	vrste jela – tip kuhinje nalik na grčku, italijansku
•	liste sastojaka
•	opisa jela
•	fotografija jela
•	cene jela
•	komentara – liste utisaka o jelu koje su korisnici ostavljali (jedan utisak se sastoji od teksta komentara, imena korisnika i datuma i vremena pisanja komentara)
*/

    private final Restaurant containingRestaurant;
    private String name;
    private Category category;
    private Type type;
    private List<String> ingredients;
    private String description;
    private List<Image> images;
    private int price;
    private List<Comment> comments;

    public Meal(String name, Category category, Type type, String description, int price, Restaurant containingRestaurant) {
        this.name = name;
        this.category = category;
        this.type = type;
        this.ingredients = new LinkedList<>();
        this.description = description;
        this.images = new LinkedList<>();
        this.price = price;
        this.comments = new LinkedList<>();
        this.containingRestaurant = containingRestaurant;
    }

    public Restaurant getContainingRestaurant() {
        return containingRestaurant;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Meal meal = (Meal) o;

        if (price != meal.price) return false;
        if (!name.equals(meal.name)) return false;
        if (!category.equals(meal.category)) return false;
        if (!type.equals(meal.type)) return false;
        if (!ingredients.equals(meal.ingredients)) return false;
        if (!description.equals(meal.description)) return false;
        if (!images.equals(meal.images)) return false;
        return comments.equals(meal.comments);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + category.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + ingredients.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + images.hashCode();
        result = 31 * result + price;
        result = 31 * result + comments.hashCode();
        return result;
    }

    public String getFullTypeName() {
        return type.getFullName();
    }
}
