package pki.desktop.model;

import javafx.scene.image.ImageView;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Restaurant {

    private String title;
    private List<Meal> meals = new LinkedList<>();
    private List<String> phones = new LinkedList<>();
    private String address;
    private Category category;
    private List<ImageView> images = new LinkedList<>();
    private Hours[] hours = new Hours[3];
    private List<Integer> grades = new LinkedList<>();
    private String description;
    private int minPrice;
    private int deliveryTime;
    private List<Comment> comments = new LinkedList<>();
    public Restaurant() {
    }
    public Restaurant(String title, String address, Category category, String description, int minPrice, int deliveryTime) {
        hours[0] = new Hours();
        hours[1] = new Hours();
        hours[2] = new Hours();
        this.title = title;
        this.address = address;
        this.category = category;
        this.description = description;
        this.minPrice = minPrice;
        this.deliveryTime = deliveryTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Hours[] getHours() {
        return hours;
    }

    public void setHours(Hours[] hours) {
        this.hours = hours;
    }

    public List<Integer> getGrades() {
        return grades;
    }

    public void setGrades(List<Integer> grades) {
        this.grades = grades;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public int getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(int deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public void addHours(Day day, LocalTime start, LocalTime end) {
        hours[day.ordinal()].enterHours(start, end);
    }

    public List<ImageView> getImages() {
        return images;
    }

    public void setImages(List<ImageView> images) {
        this.images = images;
    }

    public String getWorkdayTime() {
        return hours[0].getHHssStart() + "-" + hours[0].getHHssEnd();
    }

    public String getSaturdayTime() {
        return hours[1].getHHssStart() + "-" + hours[1].getHHssEnd();
    }

    public String getSundayTime() {
        return hours[2].getHHssStart() + "-" + hours[2].getHHssEnd();
    }

    public String getCategoryFullName() {
        return category.getFullName();
    }

    private float calculateGrade() {
        if (grades.isEmpty())
            return 0;

        float grade = 0;
        for (float g : grades)
            grade += g;
        return grade / grades.size();
    }

    public String getAverageGrade() {
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.DOWN);
        return df.format(calculateGrade());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Restaurant that = (Restaurant) o;

        return title.equals(that.title);
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }

    public boolean searchRestaurantTitle(String searchTitle) {
        if (searchTitle.trim().equals(""))
            return true;
        String[] names = this.title.split(" ");
        return Arrays.stream(names).anyMatch(s -> s.toLowerCase().startsWith(searchTitle.toLowerCase()));
    }

    private List<String> getMealNames() {
        List<String> list = new ArrayList<>();
        for (Meal m : this.meals) {
            list.add(m.getName());
        }
        return list;
    }

    public boolean searchMealName(String searchMealName) {
        if (searchMealName.trim().equals(""))
            return true;
        List<String> mealNames = getMealNames();
        for (String mealName : mealNames) {
            String[] splitNames = mealName.split(" ");
            for (String splitName : splitNames) {
                if (splitName.toLowerCase().startsWith(searchMealName.toLowerCase()))
                    return true;
            }
        }
        return false;
    }

    public boolean searchCategory(Category searchCategory) {
        if (searchCategory == Category.NONE)
            return true;
        return this.category == searchCategory;
    }

    public boolean searchType(Type searchType) {
        if (searchType == Type.NONE)
            return true;
        for (Meal m : meals) {
            if (m.getType() == searchType)
                return true;
        }
        return false;
    }

    public int getMaxMealPrice() {
        return this.meals.stream().map(Meal::getPrice).max(Integer::max).get();
    }

    public int getMinMealPrice() {
        return this.meals.stream().map(Meal::getPrice).min(Integer::min).get();
    }

    enum Day {
        WORKDAY,
        SATURDAY,
        SUNDAY
    }

    public class Hours {
        public boolean works;
        public LocalTime start, end;

        Hours() {
        }

        public Hours(LocalTime start, LocalTime end) {
            works = true;
            this.start = start;
            this.end = end;
        }

        void enterHours(LocalTime start, LocalTime end) {
            works = true;
            this.start = start;
            this.end = end;
        }

        String getHHssStart() {
            return start.format(DateTimeFormatter.ofPattern("HH:ss"));
        }

        String getHHssEnd() {
            return end.format(DateTimeFormatter.ofPattern("HH:ss"));
        }
    }
}
