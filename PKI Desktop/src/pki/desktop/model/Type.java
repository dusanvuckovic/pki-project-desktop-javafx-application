package pki.desktop.model;

public enum Type {

    NONE("Greška"),
    PIZZA("Pica"),
    PASTA("Pasta"),
    BAGEL("Bejgl"),
    GRILL("Roštilj"),
    STARTER("Predjelo");

    private final String fullName;

    Type(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }
}
